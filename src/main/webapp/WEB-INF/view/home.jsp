<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>Home</title>       
        <link rel="stylesheet" type="text/css" href="bootsrap/css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="css/home1.css"/>
        <link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css" />
        <script src="https://kit.fontawesome.com/995bfe829c.js" crossorigin="anonymous"></script>
    </head>
    <body id="top"  data-target=".navbar" data-offset="60" class="d-flex flex-column h-100">
        <!-- Nav Nar begin-->
        <div class=" bg-white  " id="home">

            <div class="container-fluid ">
                <nav class="navbar navbar-expand-md navbar-light bg-dark p-0">
                    <div class="wrapper collapse navbar-collapse justify-content-around " 
                         id="barr">




                        <ul class="social-icon">
                            <li>
                                <a href="" class="faicon-facebook rounded-circle"> <i class="fa fa-facebook " ></i></a>

                            </li>
                            <li>
                                <a href="" class="faicon-twitter rounded-circle"> <i class="fa fa-twitter " ></i></a>

                            </li>
                            <li>
                                <a href="" class="faicon-instagram rounded-circle" > <i class="fa fa-instagram " ></i></a>

                            </li>

                        </ul>
                        <ul class="navbar-nav  user1" id="usr1">


                            <li class="nav-item px-0"><a class="nav-link " href="#"> <p class="m-0"><small>GALLERY</small> </p></a></li>
                            <li class="nav-item px-0"><a class="nav-link " href="#"> <p class="m-0 "><small>CONTACT</small> </p></a></li>
                            <li class="nav-item px-0 usr"><a class="nav-link " href="#"> <p class="m-0"><i class="fas fa-user "></i> <small>${lastname}</small> </p></a></li>

                            <li class="nav-item px-0"><a class="nav-link " href="#"> <p class="m-0"><small> LOGOUT</small> </p></a></li>



                        </ul>

                    </div>


                </nav>


            </div>
            <!--End Nav -->

            <!--begin introductory-->
            <div class="container py-3 px-3  text-secondary bg-white" id="header">
                <header class="d-flex flex-row justify-content-around">

                    <h1 class="d-flex align-items-center">
                        <a href="#" class="t3"> RATS.<span class="text-success">COM</span></a>
                    </h1>

                    <h2 class="d-flex align-self-end"> <small>We Are Rats and Rats is our Business</small> </h2>
                    <h6 class="d-flex align-self-start text-success"> <a href="#contact"><small><a href="#">Support Us</a></small></a>  <a class="question"><i class="far fa-question-circle"></i></a></h6>

                </header>



            </div>

            <!--End introductory-->

            <!--Beg Body-->
            <div class="row mx-1 mb-0">

                <div class="col-md-8 pb-1 m-0 ">
                    <div class="wrapper overlay " id="cover-image">



                        <div class="hor-cen d-flex justify-content-center" id="pageintro" >
                            <article class="ml-5 pl-5  " >
                                <h2 class="heading" style="">${firstname} ${lastname}</h2>
                                <p class="text-black">Welcome to our;</p>
                                <h3 class="heading">Rats Website</h3>

                                <footer class="mt-5 pt-5">

                                    <ul m-0 p-0>
                                        <li class="d-inline-block ">
                                            <a href="#" class="btn btn-danger btn-lg">History</a>

                                        </li>

                                        <li class="d-inline-block">
                                            <a href="#" class="btn btn-outline-success btn-lg ml-4">More Infor</a>

                                        </li>

                                    </ul>

                                </footer>


                            </article>

                        </div>



                    </div>

                </div>
                <div class="col-md-4 pb-1  mb-1  " id="fprofile">
                    <div class="fprofile1 m-1" >
                        <h3>${username} Almost There! </h3>
                        <h4>Complete Below Form </h4>
                        <div class="main ">
                            <form class="bg-light p-5">

                                <div class="location ">

                                    <input class="Clocation" type="text" name="currentlocation">
                                    <label class="Clabel">Current Location</label>
                                    <input class="Harea" type="text" name="homearea">
                                    <label class="Hlabel">Home Area</label>


                                </div>
                                <div class="contact">
                                    <input class="Pnumber" type="text" name="phonenumber">
                                    <label class="Plabel">Phone Number</label>

                                </div>

                                <div class="status">

                                    <input class="profession" type="text" name="profession">
                                    <label class="Plevel">Profession</label>
                                    <input class="hustle" type="text" name="sidehustle" placeholder="hustle">
                                    <label class="Hlabel2" >Hustler</label>
                                    <select class="Elevel" name="Educationlevel" >
                                        <option disabled="disabled" selected="selected" > Choose_one</option>
                                        <option> Masters</option>
                                        <option> Bsc</option>
                                        <option>Diploma</option>
                                        <option>Certificate</option>
                                        <option> A-level</option>
                                        <option> 0-Level</option>
                                        <option>None</option>

                                    </select>

                                    <h5 class="Employed">Employed?</h5>
                                    <label class="radio">

                                        <input class="radio-one" checked="checked" type="radio" name="">
                                        <span class="checkmark"></span>

                                        Yes


                                    </label>

                                    <label class="radio">

                                        <input class="radio-two" type="radio" name="">
                                        <span class="checkmark"></span>

                                        No



                                    </label>



                                </div>

                                <input class="Vstation" type="text" name="votingstation">
                                <label class="Vlabel">Voting place</label>

                                <button class="submit" type="submit"> Submit</button>


                            </form>

                        </div>

                    </div>
                </div>

            </div>

            <!--End body-->

            <!--begin footer-->
            <div class="container-fluid bg-secondary mx-2" id="contact">

                <div class="row ">
                    <div class="col-md-8 d-flex justify-content-around  ">

                        <ul class="d-inline-block ct1 text-light">
                            <h3>Contact Us!</h3>
                            <li ><i class="fas fa-envelope-square en"></i>  <span class="text-success">rats</span>@gmail.com</li>
                            <li > <i class="fas fa-phone ph"></i> 0718885990</li> 
                        </ul>
                        <ul class="d-inline-block ct2 text-light">
                            <h5>Email Us</h5>
                            <li><i class="fas fa-envelope-square en "></i>  <input type="text" name="email" placeholder=""></li>
                            <li><i class="fas fa-phone ph"></i> <input type="text" name="number" placeholder=""></li>
                        </ul>
                    </div>
                    <div class="col-md-4 d-flex justify-content-start">

                        <ul class="social-icon align-self-end">
                            <li>
                                <a href="" class="faicon-facebook rounded-circle"> <i class="fa fa-facebook " ></i></a>

                            </li>
                            <li>
                                <a href="" class="faicon-twitter rounded-circle"> <i class="fa fa-twitter " ></i></a>

                            </li>
                            <li>
                                <a href="" class="faicon-instagram rounded-circle" > <i class="fa fa-instagram " ></i></a>

                            </li>

                        </ul>
                    </div>


                </div>



            </div>
            <!--End footer-->
        </div>

        <div class="bg-light">
            <div style="color:black; text-align: center;">
                <p> ${appName} &copy;  <small>  2008-${date},All Right Reserved. Created By: <span class="text-success">Bosco</span></small></p>
            </div>

        </div>


        
        <script type="text/javascript" src="jquery/jquery.min.js"></script>
        <script type="text/javascript" src="bootsrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="fancybox/jquery.fancybox.js"></script>  
        <script type="text/javascript" src="js/main.js"></script>
    </body>
</html>