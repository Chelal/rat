<%-- 
    Document   : mpesa_c2b
    Created on : May 18, 2021, 10:59:06 AM
    Author     : HP
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>Login</title>
        <link rel="stylesheet" type="text/css" href="bootsrap/css/bootstrap.min.css"/>
        
        <link rel="stylesheet" type="text/css" href="css/login.css"/>
        <link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css" />
    </head>
    <body>
 <header>
<h2> Mpesa C2B</h2>

</header>

   <div class="cont">  
    <div class="cent" > Fill Phone Number</div>   
    <form id='login-form' onsubmit="return false" >  
           <div class="container">
            <label>PHONE NO : </label>   
            <input type="text" placeholder="Enter phone no" name="phone" required>  
            <label>Password : </label>   
            <input type="password" placeholder="Enter Password" name="password" required>  
              <button type="submit" btn btn-large btn-primary signupbtn onclick="postDocument()">Login</button>
               Lost <a href="#" style="color:blue"> password? </a><br> Don't have an account? <a href="${pageContext.request.contextPath}/signup" style="color:green">Sign Up</a>
             
              
             			
        </div>   
    </form> 
</div>     

        

    <script type="text/javascript" src="jquery/jquery.min.js"></script>
    <script type="text/javascript" src="bootsrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="fancybox/jquery.fancybox.js"></script>  
    <script type="text/javascript" src="js/main.js"></script>
    <script type="text/javascript" >
                                function postDocument() {
                                    var str = $("#login-form").serialize();
                                    showActivity();
                                  
                                    $.ajax({
                                        type: "POST",
                                        url: "${pageContext.request.contextPath}/auth/login",
                                        data: str,
                                        success: function (rs) {
                                            hideActivity();
                                            var message = rs['message'];
                                            var status = rs['status'];
                                            if (status === 'success') {
                                                window.location.href = "${pageContext.request.contextPath}/home";
                                            } else {
                                                fancyAlert(message);
                                            }
                                        },
                                        error: function (msg) {
                                            fancyAlert('An Internal System error occured.');
                                        }
                                    });


                                }


    </script>

</body>
</html>
