<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>login</title>
        <link rel="stylesheet" type="text/css" href="bootsrap/css/bootstrap.min.css"/> 
        <link rel="stylesheet" type="text/css" href="css/main.css"/>
        <link rel="stylesheet" type="text/css" href="css/logout.css"/>
        <link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css" />
    </head>
    <body>
        <div class="container mt-1 con">
            <div class="row">
                <h1>Sign Up Form</h1>
                        <p style="text-align: center; padding:0;margin-top:1px">Rat! fill in this form to create an account.</p>
                        <hr>

                <form class="mt-0" id='signup-form' onsubmit="return false" style="border:1px solid #ccc">
                    <div class="mt-0">
                        <label for="email"><b>Firstname</b></label>
                        <input type="text" placeholder="Enter Firstname" name="firstname" required>
                        <label for="email"><b>Lastname</b></label>
                        <input type="text" placeholder="Enter Lastname" name="lastname" required>
                        <label for="email"><b>Username</b></label>
                        <input type="text" placeholder="Enter Username" name="username" required>
                        <label for="email"><b>Gender</b></label>
                        <input type="text" placeholder="Enter gender" name="gender" required>
                        <label for="email"><b>Email</b></label>
                        <input type="text" placeholder="Enter Email" name="email" required>

                        <label for="psw"><b>Password</b></label>
                        <input type="password" placeholder="Enter Password" name="password1" required>



                        <label>
                            <input type="checkbox" checked="checked" name="remember" style="margin-bottom:15px"> Remember me
                        </label>

                        <p>By creating an account you agree to our <a href="#" style="color:dodgerblue">Terms & Privacy</a>.</p>
                         <p>You have an Account? <a href="${pageContext.request.contextPath}" style="color:dodgerblue">Signin</a>.</p>
                        <div class=" d-flex justify-content-center ">

                            <button class="btn btn-large btn-primary signupbtn " type="submit"  onclick="postDocument()">Sign Up</button>
                        </div>
                    </div>
                </form>

            </div>




            <div class="row">
                <p class="text-muted">
                    ${appName}&copy; 2008-${date}, All Rights Reserved . 
                </p>
            </div>

        </div>



        <script type="text/javascript" src="jquery/jquery.min.js"></script>
        <script type="text/javascript" src="bootsrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="fancybox/jquery.fancybox.js"></script>  
        <script type="text/javascript" src="js/main.js"></script>
        <script type="text/javascript" >
                                function postDocument() {
                                    var str = $("#signup-form").serialize();

                                 
                                    showActivity();
                                    $.ajax({
                                        type: "POST",
                                        url: "${pageContext.request.contextPath}/save",
                                        data: str,
                                        timeout: 100000,
                                        success: function (rs) {
                                            hideActivity();
                                            var message = rs['message'];
                                            var status = rs['status'];
                                            if (status === 'success') {
                                                window.location.href = "${pageContext.request.contextPath}";
                                            } else {
                                                fancyAlert(message);
                                            }
                                        },
                                        error: function (msg) {
                                            fancyAlert('An Internal System error occured.');
                                        }
                                    });


                                }


        </script>


    </body>

</html>