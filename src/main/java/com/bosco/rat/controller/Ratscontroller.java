/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bosco.rat.controller;

import com.bosco.rat.RatApplication;
import com.bosco.rat.entity.User;
import com.bosco.rat.repository.UserRepository;
import java.util.Calendar;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author HP
 */
@Controller
@RequestMapping("/")
public class Ratscontroller {

    Logger logger = LogManager.getLogger();
    @Autowired
    private
            UserRepository repo;

    public UserRepository getRepo() {
        return repo;
    }

    public void setRepo(UserRepository repo) {
        this.repo = repo;
    }
    
    public Ratscontroller(UserRepository repo) {
        this.repo = repo;
    }

    @Value("${spring.application.name}")
    String appName;
    
    @GetMapping("")
    public String index(Model model) {
        model.addAttribute("appName", appName);
        
        model.addAttribute("date", Calendar.getInstance().get(Calendar.YEAR));

        return "login";

    }

    @GetMapping("sign")
    public String sign(HttpServletRequest request, Model model) {
        String username = request.getParameter("user");
        model.addAttribute("user", username);

        return "usersignup";
    }

    @GetMapping("signup")
    public String signup(Model model) {
        model.addAttribute("appName", appName);
        model.addAttribute("date", Calendar.getInstance().get(Calendar.YEAR));
     
        return "usersignup";
    }

    @RequestMapping(value = "save", method = RequestMethod.POST, produces = {"application/JSON"})
    @ResponseBody
    public HashMap Saverat(HttpServletRequest request, Model model) {
        String username =request.getParameter("username");
        String firstname = request.getParameter("firstname");
        String lastname = request.getParameter("lastname");
        String email = request.getParameter("email");
        String gender = request.getParameter("gender");
        String password = request.getParameter("password1");
    
        
        //UserRepository repo = RatApplication.context.getBean(UserRepository.class);
        User user = repo.findByUsername(username);
        if (user == null) {
                user = new User();
            }
        //System.out.println(user.getUsername());
        System.out.println(lastname);
        System.out.println(email);
        System.out.println(gender);
        System.out.println(username);
        System.out.println(password); 
        System.out.println(firstname);
                
        HashMap hashmap = new HashMap();
        
        
          System.out.println(user.getUsername());
         System.out.println(user);
        
       
       if (StringUtils.equals(username,user.getUsername())) {
            hashmap.put("status", "fail");
            hashmap.put("message", "Rat with the same username exit! choose another");

        } else {
        
            User userr = new User();
            userr.setFirstname(firstname);
            userr.setLastname(lastname);
            userr.setUsername(username);
            userr.setEmail(email);
            userr.setGender(gender);
            userr.setPassword(password);
            repo.save(userr);

            hashmap.put("status", "success");
            hashmap.put("message", "You sign up successfully!!!! Welcome!");

        } 
        
       
        return hashmap;
    }

    @GetMapping("home")
    public String home(HttpServletRequest request, Model model) {
        model.addAttribute("appName", appName);
      
        HttpSession session = request.getSession(true);
        User user = (User) session.getAttribute("users");
        model.addAttribute("date", Calendar.getInstance().get(Calendar.YEAR));
        model.addAttribute("firstname", user.getFirstname());
        model.addAttribute("lastname", user.getLastname());
        model.addAttribute("username", user.getUsername());
        model.addAttribute("email", user.getEmail());
        model.addAttribute("gender", user.getGender());

        return "home";
    }
    
     @GetMapping("mpesano")
    public String mpesano(Model model) {
        model.addAttribute("appName", appName);
        model.addAttribute("date", Calendar.getInstance().get(Calendar.YEAR));

        return "mpesa_c2b";

    }

}
