/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bosco.rat.controller;

import com.bosco.rat.entity.User;
import com.bosco.rat.repository.UserRepository;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author HP
 */
@RestController
@RequestMapping("/auth")
public class Authenticationcontroller {

    Logger logger = LogManager.getLogger();
    
    @Autowired
    UserRepository userRepository;
    
    public Authenticationcontroller(UserRepository userRepository){
        this.userRepository=userRepository;
    }
  
    @RequestMapping(value = "login", method = RequestMethod.POST, produces = "application/json")
    public HashMap Login(HttpServletRequest request) {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        HashMap hashmap = new HashMap();
        if (StringUtils.isBlank(username) || StringUtils.isBlank(password)) {
            hashmap.put("status", "fail");
            hashmap.put("message", "username or password is a must.Now Blank");
        } else {
            User user = userRepository.findByUsername(username);
            if (user==null){
            user=new User();
            }
            if (StringUtils.equals(password,user.getPassword())&& StringUtils.equals(username,user.getUsername())) {
                HttpSession session=request.getSession(true);
                session.setAttribute("users", user);
                hashmap.put("status", "success");
                hashmap.put("message", "you are succesfully logged in");
            } else {
                hashmap.put("status", "fail");
                hashmap.put("message", "wrong username or password");

            }

        }

        return hashmap;
    }

}
