/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bosco.rat.controller;


import static com.bosco.rat.RatApplication.logs;
import com.bosco.rat.b2c.dtos.internalB2CTransactionRequest;
import com.bosco.rat.dtos.AccessTokenResponse;

import com.bosco.rat.dtos.AknowledgeTransaction;
import com.bosco.rat.dtos.B2CResponseAsynTransaction;
import com.bosco.rat.dtos.B2CResponseTransaction;
import com.bosco.rat.dtos.C2BRequestSimulation;
import com.bosco.rat.dtos.C2BResponseSimulation;
import com.bosco.rat.dtos.MpesaValidationUrl;
import com.bosco.rat.dtos.RegisterUrlResponse;
import com.bosco.rat.services.MpesaDarajaApi;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author HP
 */
@Data
@RestController
@RequestMapping("/t1")
public class Mpesacontroller {
    @Autowired
    private final MpesaDarajaApi mpesaDarajaApi;
    @Autowired
    private final ObjectMapper objectMapper;

    public Mpesacontroller(MpesaDarajaApi mpesaDarajaApi, ObjectMapper objectMapper, AknowledgeTransaction aknowledgeTransaction) {
        this.mpesaDarajaApi = mpesaDarajaApi;
        this.objectMapper = objectMapper;
        this.aknowledgeTransaction = aknowledgeTransaction;
    }
    @Autowired
    private final AknowledgeTransaction aknowledgeTransaction;

   
    @GetMapping(path = "/token", produces = "application/json")//done
    public ResponseEntity<AccessTokenResponse> getAccessToken() {
        return ResponseEntity.ok(mpesaDarajaApi.getAccessToken());
    }
     @PostMapping(path = "/register-url", produces = "application/json")//done
    public ResponseEntity<RegisterUrlResponse> getUrlResponse() {
        return ResponseEntity.ok(mpesaDarajaApi.getUrlResponse());
    }
    
     @PostMapping(path = "/validation", produces = "application/json")
    public ResponseEntity<AknowledgeTransaction> getValidationUrl(@RequestBody MpesaValidationUrl mpesaValidationUrl) {
        return ResponseEntity.ok(aknowledgeTransaction);
    }
    @PostMapping(path = "/c2b-transaction-simulation", produces = "application/json")
    public ResponseEntity<C2BResponseSimulation> getC2bSimulation(@RequestBody C2BRequestSimulation C2BRequest) {
        return ResponseEntity.ok(mpesaDarajaApi.getC2BTransactionResponse(C2BRequest));//done
    }
      @PostMapping(path = "/b2c-transaction-result", produces = "application/json")
    public ResponseEntity<AknowledgeTransaction> b2cTransactionAsyncResults(@RequestBody B2CResponseAsynTransaction b2CTransactionAsyncResponse)
            throws JsonProcessingException {
        logs.info("============ B2C Transaction Response =============");
        logs.info(objectMapper.writeValueAsString(b2CTransactionAsyncResponse));
        return ResponseEntity.ok(aknowledgeTransaction);
    }

    @PostMapping(path = "/b2c-queue-timeout", produces = "application/json")
    public ResponseEntity<AknowledgeTransaction> queueTimeout(@RequestBody Object object) {
        return ResponseEntity.ok(aknowledgeTransaction);
    }

    @PostMapping(path = "/b2c-transaction", produces = "application/json")
    public ResponseEntity<B2CResponseTransaction > performB2CTransaction(@RequestBody internalB2CTransactionRequest  internalB2CTransactionRequest) {
        return ResponseEntity.ok(mpesaDarajaApi.performB2CTransaction(internalB2CTransactionRequest));
    }
     @PostMapping(path = "/stk-transaction", produces = "application/json")
    public ResponseEntity<STKPushResponseTransaction> performSTKTransaction(@RequestBody internalSTKTransactionRequest  internalStkTransactionRequest) {
        return ResponseEntity.ok(mpesaDarajaApi.performSTKTransaction(internalStkTransactionRequest));
    }
    @PostMapping(path = "/stkpushresult", produces = "application/json")
    public ResponseEntity<AknowledgeTransaction> getSTKPushCallback(@RequestBody STKPushSycResultRespose stkPushSycResultRespose) {
        return ResponseEntity.ok(aknowledgeTransaction);
    }
}

