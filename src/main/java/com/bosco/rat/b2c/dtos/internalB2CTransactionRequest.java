/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bosco.rat.b2c.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 *
 * @author HP
 */
@Data
public class internalB2CTransactionRequest {
     @JsonProperty("Amount") 
    public String amount;
     @JsonProperty("Remarks") 
    public String remarks;
     @JsonProperty("CommandID") 
    public String commandID;
     @JsonProperty("PartyB") 
    public String partyB;
     @JsonProperty("Occassion") 
    public String occassion;
}
