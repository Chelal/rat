/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bosco.rat.b2c.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.Data;

/**
 *
 * @author HP
 */
@Data
public class ResultParameterData {
    @JsonProperty("ResultParameter") 
    public List<ResultParameterItem> resultParameter;  
}
