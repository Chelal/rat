/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bosco.rat.b2c.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 *
 * @author HP
 */
@Data
public class Results {
     @JsonProperty("ResultType") 
    public int resultType;
    @JsonProperty("ResultCode") 
    public int resultCode;
    @JsonProperty("ResultDesc") 
    public String resultDesc;
    @JsonProperty("OriginatorConversationID") 
    public String originatorConversationID;
    @JsonProperty("ConversationID") 
    public String conversationID;
    @JsonProperty("TransactionID") 
    public String transactionID;

    /**
     *
     */
    @JsonProperty("ResultParameters") 
    public ResultParameterData resultParameters;
    @JsonProperty("ReferenceData") 
    public ResultReferenceData referenceData;
}
