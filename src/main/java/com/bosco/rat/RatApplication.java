package com.bosco.rat;

import com.bosco.rat.dtos.AknowledgeTransaction;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zaxxer.hikari.HikariDataSource;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileLock;
import java.sql.SQLException;
import okhttp3.OkHttpClient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
//import org.springframework.context.ConfigurableApplicationContext;


@SpringBootApplication
public class RatApplication extends SpringBootServletInitializer {
    
     @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(RatApplication.class);
    }
//public static ConfigurableApplicationContext context;
    public String billerDBSchema;
    public static HikariDataSource billerDBDataSource;
    public static ConfigurableApplicationContext context;
    @Value("${spring.datasource.url}")
    private String billerUrl;
    @Value("${spring.datasource.username}")
    private String billerUsername;
    @Value("${spring.datasource.password}")
    private String billerPassword;
   
  public static Logger logs = LogManager.getLogger(RatApplication.class);
  
    
    public static void main(String[] args) {
        
        try{
		context=SpringApplication.run(RatApplication.class, args);
                RatApplication.isRunning();
                logs.info("Rats Application Is Running");
        } catch(Exception e){  logs.error("Ex", e);
        }
	}
    @Bean
   public OkHttpClient getOkHttpClient(){
       return new OkHttpClient();
   
   }
     @Bean
   public ObjectMapper getObjectMapper(){
       return new ObjectMapper();
   
   }
   
    @Bean
   public AknowledgeTransaction getAknowledgeTransaction(){
       AknowledgeTransaction aknowledgeTransaction=new AknowledgeTransaction();
       aknowledgeTransaction.setSuccess("Success");
       return aknowledgeTransaction;
   
   }
    @Bean
    public HikariDataSource BillerdataSource() throws SQLException {
        
        billerDBDataSource = new HikariDataSource();
        billerDBDataSource.setUsername(billerUsername);
        
        billerDBDataSource.setPassword((billerPassword));
        billerDBDataSource.setJdbcUrl(billerUrl);
      
      
        return billerDBDataSource;
    }
public static void isRunning() {
        try {
            final File file = new File(".text");
            final RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
            final FileLock fileLock = randomAccessFile.getChannel().tryLock();
            logs.info("Acquiring Lock\n=================================================");
            logs.info(fileLock.toString());
        } catch (IOException ex) {
            logs.info("Failed Acquiring Lock, Another instance is running");
            System.exit(1);
        }
    }
	
}

