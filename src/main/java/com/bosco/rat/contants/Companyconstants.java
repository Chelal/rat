/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bosco.rat.contants;

import okhttp3.MediaType;


/**
 *
 * @author HP
 */
public class Companyconstants {
    public static String FAMILY_NAME = "Rats Family";
    public static int DB_VERSION = 2021;
    public static String SYSTEM_NAME = "Rats database Systems";
    public static String EMAIL = "rats001@gmail.com";
    public static String COMPANY_LICENCE_NO = "";
    public static String LICENCE_EXPIRY_DATE = "30/12/2099 00:00";
    public static String COMPANY_ADVERTISINGMSG = "System By Chelal";
    public static final String BASIC_AUTH_STRING = "Basic";
    public static final String AUTHORIZATION_HEADER_STRING = "authorization";
    public static final String BEARER_AUTH_STRING = "Bearer";

    public static final String CACHE_CONTROL_HEADER = "cache-control";
    public static final String CACHE_CONTROL_HEADER_VALUE = "no-cache";
    public static MediaType JSON_MEDIA_TYPE = MediaType.parse("application/json; charset=utf-8");
}                                                             
