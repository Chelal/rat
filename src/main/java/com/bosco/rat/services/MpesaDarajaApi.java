/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bosco.rat.services;

import com.bosco.rat.b2c.dtos.internalB2CTransactionRequest;
import com.bosco.rat.dtos.AccessTokenResponse;

import com.bosco.rat.dtos.B2CResponseTransaction;
import com.bosco.rat.dtos.C2BRequestSimulation;
import com.bosco.rat.dtos.C2BResponseSimulation;
import com.bosco.rat.dtos.RegisterUrlRequest;
import com.bosco.rat.dtos.RegisterUrlResponse;



/**
 *
 * @author HP
 */
public interface MpesaDarajaApi {
    
    /**
     * @return Returns Daraja API Access Token Response
     */
    AccessTokenResponse getAccessToken();
    RegisterUrlResponse getUrlResponse();
    C2BResponseSimulation getC2BTransactionResponse(C2BRequestSimulation C2BRequest);
    B2CResponseTransaction performB2CTransaction(internalB2CTransactionRequest  internalB2CTransactionRequest);
}
