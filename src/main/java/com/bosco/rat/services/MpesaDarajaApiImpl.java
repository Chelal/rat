/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bosco.rat.services;

import static com.bosco.rat.RatApplication.logs;
import com.bosco.rat.b2c.dtos.internalB2CTransactionRequest;
import com.bosco.rat.config.MpesaConfiguration;
import static com.bosco.rat.contants.Companyconstants.AUTHORIZATION_HEADER_STRING;
import static com.bosco.rat.contants.Companyconstants.BASIC_AUTH_STRING;
import static com.bosco.rat.contants.Companyconstants.BEARER_AUTH_STRING;
import static com.bosco.rat.contants.Companyconstants.CACHE_CONTROL_HEADER;
import static com.bosco.rat.contants.Companyconstants.CACHE_CONTROL_HEADER_VALUE;
import static com.bosco.rat.contants.Companyconstants.JSON_MEDIA_TYPE;
import com.bosco.rat.dtos.AccessTokenResponse;



import com.bosco.rat.dtos.B2CRequestTransaction;
import com.bosco.rat.dtos.B2CResponseTransaction;
import com.bosco.rat.dtos.C2BRequestSimulation;
import com.bosco.rat.dtos.C2BResponseSimulation;
import com.bosco.rat.dtos.RegisterUrlRequest;
import com.bosco.rat.dtos.RegisterUrlResponse;


import com.bosco.rat.utility.HelperUnility;
import static com.bosco.rat.utility.HelperUnility.getSecurityCredentials;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import okhttp3.OkHttpClient;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author HP
 */
import lombok.Data;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.springframework.stereotype.Service;

@Service
@Data
public class MpesaDarajaApiImpl implements MpesaDarajaApi{
    @Autowired
    private final MpesaConfiguration mpesaConfiguration;
     @Autowired
    private final OkHttpClient okHttpClient;
      @Autowired
    private final ObjectMapper objectMapper;

    public MpesaDarajaApiImpl(MpesaConfiguration mpesaConfiguration, OkHttpClient okHttpClient, ObjectMapper objectMapper) {
        this.mpesaConfiguration = mpesaConfiguration;
        this.okHttpClient = okHttpClient;
        this.objectMapper = objectMapper;
        
    }
  
    @Override
    public AccessTokenResponse getAccessToken() {
     //To change body of generated methods, choose Tools | Templates.
        // get the Base64 rep of consumerKey + ":" + consumerSecret
      
        HelperUnility HelperUnility =new HelperUnility();
        String encodedCredentials = HelperUnility.tobase64String(String.format("%s:%s", mpesaConfiguration.getKey(),
                mpesaConfiguration.getSecret()));
         
        Request request = new Request.Builder()
                .url(mpesaConfiguration.getOauthEndpoint())
                .get()
                .addHeader(AUTHORIZATION_HEADER_STRING, String.format("%s %s", BASIC_AUTH_STRING, "cjhZR1A4WmR5UEFpcVhENldMcm1TYTlXcHlkY1pEeXI6cmwyMEs0ckFJWW1BQ2dnSQ=="))
                .addHeader(CACHE_CONTROL_HEADER, CACHE_CONTROL_HEADER_VALUE)
                .build();

        try {
            Response response = okHttpClient.newCall(request).execute();
           
            assert response.body() != null;

            // use Jackson to Decode the ResponseBody ...
           
            //return objectMapper.readValue(response.body().string(), AccessTokenResponse.class);
            return objectMapper.readValue(response.body().string(),AccessTokenResponse.class);
        } catch (IOException e) {
        
            logs.info(String.format("Could not get access token. -> %s", e.getLocalizedMessage()));
            return null;
        }
    }

    @Override
    public RegisterUrlResponse getUrlResponse() {
      AccessTokenResponse accessResponse=getAccessToken();
      RegisterUrlRequest registerUrlRequest=new RegisterUrlRequest();
      registerUrlRequest.setShortCode(mpesaConfiguration.getShortCode());
      registerUrlRequest.setResponseType(mpesaConfiguration.getResponseType());
      registerUrlRequest.setValidationURL(mpesaConfiguration.getValidationUrl());
      registerUrlRequest.setConfirmationUrl(mpesaConfiguration.getConfirmationUrl());
       
       RequestBody body = RequestBody.create(Objects.requireNonNull(HelperUnility.toJson(registerUrlRequest)),JSON_MEDIA_TYPE
                );
       
         Request request = new Request.Builder()
                .url(mpesaConfiguration.getRegisterUrl())
                .post(body)
                .addHeader("Authorization", String.format("%s %s", BEARER_AUTH_STRING,accessResponse.getAccessToken() ))
                .build();
          
        try {
            Response response = okHttpClient.newCall(request).execute();
             assert response.body() != null;
            return objectMapper.readValue(response.body().string(), RegisterUrlResponse.class);
        } catch (IOException e) {
            logs.info(String.format("Could not get url -> %s", e.getLocalizedMessage()));
            return null;
          
        }
       
      
      
      
      
    }

    @Override
    public C2BResponseSimulation getC2BTransactionResponse(C2BRequestSimulation C2BRequest) {
        RequestBody body = RequestBody.create(Objects.requireNonNull(HelperUnility.toJson(C2BRequest)),JSON_MEDIA_TYPE);
        AccessTokenResponse accessResponse=getAccessToken();
         Request request = new Request.Builder()
                .url(mpesaConfiguration.getSimulationUrl())
                .post(body)
                .addHeader("Authorization", String.format("%s %s", BEARER_AUTH_STRING, accessResponse.getAccessToken()))
                .build();
          
        try {
            Response response = okHttpClient.newCall(request).execute();
             assert response.body() != null;
            return objectMapper.readValue(response.body().string(), C2BResponseSimulation.class);
        } catch (IOException e) {
            logs.info(String.format("Could not get access C2BSImulation URL -> %s", e.getLocalizedMessage()));
            return null;
          
        }
    }

    @Override
    public B2CResponseTransaction performB2CTransaction(internalB2CTransactionRequest internalB2CTransactionRequest) {
      B2CRequestTransaction request = new B2CRequestTransaction();
      AccessTokenResponse accessResponse=getAccessToken();
      request.setAmount(internalB2CTransactionRequest.getAmount());
      request.setCommandID(internalB2CTransactionRequest.getCommandID());
      request.setOccassion(internalB2CTransactionRequest.getOccassion());
      request.setRemarks(internalB2CTransactionRequest.getRemarks());
      request.setPartyB(internalB2CTransactionRequest.getPartyB());
      request.setInitiatorName(mpesaConfiguration.getB2CinitiatorName());
      request.setSecurityCredential(getSecurityCredentials(mpesaConfiguration.getB2CinitiatorPassword()));
      request.setQueueTimeOutURL(mpesaConfiguration.getB2CQueueTimeOutUrl());
      request.setResultURL(mpesaConfiguration.getB2CResultUrl());
      request.setPartyA(mpesaConfiguration.getShortCode());
      logs.info(request.toString());
     /*OkHttpClient client = new OkHttpClient().newBuilder().build();
MediaType mediaType = MediaType.parse("application/json");
RequestBody body = RequestBody.create(mediaType, {
    "InitiatorName": "testapi",
    "SecurityCredential": "AGJrGvU4YUI1HVPwBwr6fpapcUNGQlVcHzElzMHYokGVd4ZoVq/EbfBW+aCtT3nLWIQi/CZGi1XPTLGFvTh+daaHh5AxpFONy28e8spn2SbcbFSdUb2bmrGP0okPR2tviZ3rvzHIZKkk2RN7+ux+3URavQ0vpdnTT2TiYD242P8x8b33a2rqETMQAFCbQtqwoRxIsDsI7K/9ystNnytTJxfvM6MiQ5W6bXzrq7vdHKqL5LDFbTV45r/Q8oVXPlBzsAl0B4Y6wmA/35MpJEuboa+/pY9K94wEpcF3ZkV+fpDttXxDzV1AxiEyPwmhAQRSxjUzhj7SNcjVjIaZPLfaWg==",
    "CommandID": "BusinessPayment",
    "Amount": 1,
    "PartyA": 600983,
    "PartyB": 254708374149,
    "Remarks": "Test remarks yes",
    "QueueTimeOutURL": "https://183218701da9.ngrok.io/rat/mobile-money/b2c-queue-timeout",
    "ResultURL": "https://183218701da9.ngrok.io/rat/mobile-money/b2c-transaction-result",
    "Occassion": "birthday",
  });
Request request = new Request.Builder()
  .url("https://sandbox.safaricom.co.ke/mpesa/b2c/v1/paymentrequest")
  .method("POST", body)
  .addHeader("Content-Type", "application/json")
  .addHeader("Authorization", "Bearer zaiDj3mPvpSJbfVAyk8VRHIcPlMV")
  .build();
Response response = client.newCall(request).execute()*/
      
    RequestBody body = RequestBody.create(Objects.requireNonNull(HelperUnility.toJson(request)),JSON_MEDIA_TYPE
                );
    Request req = new Request.Builder()
                .url(mpesaConfiguration.getB2CTransactionUrl())
                .post(body)
                .addHeader("Content-Type", "application/json")
                .addHeader("Authorization", String.format("%s %s", BEARER_AUTH_STRING, accessResponse.getAccessToken()))
                .build();
     Response response;
        try {
            response = okHttpClient.newCall(req).execute();
            assert response.body() != null;
            return objectMapper.readValue(response.body().string(), B2CResponseTransaction.class);
        } catch (IOException ex) {
            logs.info(String.format("Could not get access B2CTransaction URL-> %s", ex.getLocalizedMessage()));
             return null;
        }
             
      
      
    }
   
}
