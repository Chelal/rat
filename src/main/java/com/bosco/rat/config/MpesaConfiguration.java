/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bosco.rat.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author HP
 */
@Configuration
@ConfigurationProperties(prefix="daraja.api")
public class MpesaConfiguration {
    private String key;
    private String secret;
    private String grantType;
    private Integer shortCode;

    public Integer getShortCode() {
        return shortCode;
    }

    public void setShortCode(Integer shortCode) {
        this.shortCode = shortCode;
    }
    private String oauthEndpoint;
    private String responseType;
     private String confirmationUrl;
    private String validationUrl;
    private String registerUrl; 
    private String simulationUrl;
    private String B2CTransactionUrl;
    private String B2CResultUrl;
    private String B2CQueueTimeOutUrl;
    private String B2CinitiatorName;
    private String B2CinitiatorPassword;

    public String getB2CinitiatorName() {
        return B2CinitiatorName;
    }

    public void setB2CinitiatorName(String B2CinitiatorName) {
        this.B2CinitiatorName = B2CinitiatorName;
    }

    public String getB2CinitiatorPassword() {
        return B2CinitiatorPassword;
    }

    public void setB2CinitiatorPassword(String B2CinitiatorPassword) {
        this.B2CinitiatorPassword = B2CinitiatorPassword;
    }
    

    public String getB2CTransactionUrl() {
        return B2CTransactionUrl;
    }

    public void setB2CTransactionUrl(String B2CTransactionUrl) {
        this.B2CTransactionUrl = B2CTransactionUrl;
    }

    public String getB2CResultUrl() {
        return B2CResultUrl;
    }

    public void setB2CResultUrl(String B2CResultUrl) {
        this.B2CResultUrl = B2CResultUrl;
    }

    public String getB2CQueueTimeOutUrl() {
        return B2CQueueTimeOutUrl;
    }

    public void setB2CQueueTimeOutUrl(String B2CQueueTimeOutUrl) {
        this.B2CQueueTimeOutUrl = B2CQueueTimeOutUrl;
    }

    public String getSimulationUrl() {
        return simulationUrl;
    }

    public void setSimulationUrl(String simulationUrl) {
        this.simulationUrl = simulationUrl;
    }
    public String getRegisterUrl() {
        return registerUrl;
    }

    public void setRegisterUrl(String registerUrl) {
        this.registerUrl = registerUrl;
    }

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }
    
    
    
   

 

   

    public String getConfirmationUrl() {
        return confirmationUrl;
    }

    public void setConfirmationUrl(String confirmationUrl) {
        this.confirmationUrl = confirmationUrl;
    }

    public String getValidationUrl() {
        return validationUrl;
    }

    public void setValidationUrl(String validationUrl) {
        this.validationUrl = validationUrl;
    }
   

    @Override
    public String toString() {
        return String.format("{consumerKey='%s', consumerSecret='%s', grantType='%s', oauthEndpoint='%s'}",
                key, secret, grantType, oauthEndpoint);
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getGrantType() {
        return grantType;
    }

    public void setGrantType(String grantType) {
        this.grantType = grantType;
    }

    public String getOauthEndpoint() {
        return oauthEndpoint;
    }

    public void setOauthEndpoint(String oauthEndpoint) {
        this.oauthEndpoint = oauthEndpoint;
    }
}
