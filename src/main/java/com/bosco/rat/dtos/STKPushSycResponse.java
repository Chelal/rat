/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bosco.rat.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 *
 * @author HP
 */
@Data
public class STKPushSycResponse {
       @JsonProperty("MerchantRequestID") 
    public String merchantRequestID;
    @JsonProperty("CheckoutRequestID") 
    public String checkoutRequestID;
    @JsonProperty("ResponseCode") 
    public String responseCode;
    @JsonProperty("ResponseDescription") 
    public String responseDescription;
    @JsonProperty("CustomerMessage") 
    public String customerMessage; 
}
