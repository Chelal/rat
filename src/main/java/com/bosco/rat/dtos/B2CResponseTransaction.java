/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bosco.rat.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author HP
 */
public class B2CResponseTransaction {
    @JsonProperty("ConversationID") 
    public String conversationID;
    @JsonProperty("OriginatorConversationID") 
    public String originatorConversationID;
    @JsonProperty("ResponseCode") 
    public String responseCode;
    @JsonProperty("ResponseDescription") 
    public String responseDescription;
   
}
