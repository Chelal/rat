/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bosco.rat.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 *
 * @author HP
 */
@Data
public class MpesaValidationUrl {
   @JsonProperty("TransactionType")
        private String TransactionType; 
   @JsonProperty("TransID")
        private String TransID; 
   @JsonProperty("TransTime")
        private String TransTime; 
   @JsonProperty("TransAmount")
        private String TransAmount; 
   @JsonProperty("BusinessShortCode")
        private String BusinessShortCode; 
   @JsonProperty("BillRefNumber")
        private String BillRefNumber; 
   @JsonProperty("InvoiceNumber")
        private String InvoiceNumber;
   @JsonProperty("OrgAccountBalance")
        private String OrgAccountBalance;
   @JsonProperty("ThirdPartyTransID")
        private String ThirdPartyTransID;
   @JsonProperty("MSISDN")
        private String MSISDN;
   @JsonProperty("FirstName")
        private String FirstName;
   @JsonProperty("MiddleName")
        private String MiddleName;
   @JsonProperty("LastName")
        private String LastName;
}

  
  



