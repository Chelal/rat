/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bosco.rat.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 *
 * @author HP  
 */
@Data
public class RegisterUrlResponse {
     @JsonProperty("ResponseCode")
        private String ResponseCode;
      @JsonProperty("OriginatorCoversationID")
        private String originatorCoversationID;
       @JsonProperty("ResponseDescription")
        private String responseDescription;
       
}

