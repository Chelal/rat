/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bosco.rat.dtos;

import com.bosco.rat.b2c.dtos.Results;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 *
 * @author HP
 */
@Data
public class B2CResponseAsynTransaction {
    @JsonProperty("Result") 
    public Results result;
    
}
