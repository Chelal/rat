/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bosco.rat.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 *
 * @author HP
 */
@Data
public class B2CRequestTransaction {
    @JsonProperty("InitiatorName") 
    public String initiatorName;
    @JsonProperty("SecurityCredential") 
    public String securityCredential;
    @JsonProperty("CommandID") 
    public String commandID;
    @JsonProperty("Amount") 
    public String amount;
    @JsonProperty("PartyA") 
    public Integer partyA;
    @JsonProperty("PartyB") 
    public String partyB;
    @JsonProperty("Remarks") 
    public String remarks;
    @JsonProperty("QueueTimeOutURL") 
    public String queueTimeOutURL;
    @JsonProperty("ResultURL") 
    public String resultURL;
    @JsonProperty("Occassion") 
    public String occassion;
}
