/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bosco.rat.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 *
 * @author HP
 */
@Data
public class RegisterUrlRequest {
          @JsonProperty("ShortCode")
        private Integer shortCode;  
          @JsonProperty("ResponseType")
        private String responseType;  
          @JsonProperty("ValidationURL")
        private String validationURL; 
          
          @JsonProperty("ConfirmationURL")
        private String ConfirmationUrl; 
}
