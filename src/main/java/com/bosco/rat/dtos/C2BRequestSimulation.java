/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bosco.rat.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 *
 * @author HP
 */
@Data
public class C2BRequestSimulation {
    @JsonProperty("ShortCode")
        private Integer shortCode;
    @JsonProperty("Amount")
        private String Amount;
    @JsonProperty("CommandID")
        private String commandID;
    @JsonProperty("Msisdn")
        private String Msisdn;
    @JsonProperty("BillRefNumber")
        private String BillRefNumber;
}




 
   