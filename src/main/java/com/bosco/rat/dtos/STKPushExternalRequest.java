/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bosco.rat.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 *
 * @author HP
 */
@Data
public class STKPushExternalRequest {
    @JsonProperty("BusinessShortCode") 
    public String businessShortCode;
    @JsonProperty("Password") 
    public String password;
    @JsonProperty("Timestamp") 
    public String timestamp;
    @JsonProperty("TransactionType") 
    public String transactionType;
    @JsonProperty("Amount") 
    public String amount;
    @JsonProperty("PartyA") 
    public String partyA;
    @JsonProperty("PartyB") 
    public String partyB;
    @JsonProperty("PhoneNumber") 
    public String phoneNumber;
    @JsonProperty("CallBackURL") 
    public String callBackURL;
    @JsonProperty("AccountReference") 
    public String accountReference;
    @JsonProperty("TransactionDesc") 
    public String transactionDesc;
}
