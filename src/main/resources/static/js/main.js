const showActivity = () => {
    $.fancybox.open('<div class="spinner-border text-primary" role="status"><span class="visually-hidden">Loading...</span></div>');
};

const fancyAlert = (content) => {
    $.fancybox.open(content);
};

const hideActivity = () => {
    parent.jQuery.fancybox.getInstance().close();
};
